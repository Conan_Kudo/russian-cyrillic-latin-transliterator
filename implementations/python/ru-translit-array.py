# Library for transliterating between Cyrillic and Latin for Russian
# Author: Neal Gompa
# Licensed under GNU GPL version 3 or any later version
# SPDX-License-Identifier: GPL-3.0+
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys

ruCyrlArray = [ 'А','а','Б','б','В','в','Г','г','Д','д','Е','е','Ё','ё','Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш','Щ','щ','ъ','Ъ','Ы','ы','ь','Ь','Э','э','Ю','ю','Я','я' ]

ruLatnArray = [ 'A','a','B','b','V','v','G','g','D','d','È','è','Ò','ò','Ž','ž','Z','z','I','i','J','j','K','k','L','l','M','m','N','n','O','o','P','p','R','r','S','s','T','t','U','u','F','f','H','h','C','c','Č','č','Š','š','Ŝ','ŝ','″','″','Y','y','′','′','E','e','Ù','ù','À','à' ]

ruLatnExpArray = [ 'C′','c′','Č′','č′','D′','d′','G′','g′','K′','k′','L′','l′','M′','m′','N′','n′','P′','p′','R′','r′','S′','s′','Š′','š′','Ŝ′','ŝ′','T′','t′','Z′','z′','Ž′','ž′' ]

ruLatnCondArray = [ 'Ć','ć','Ĉ','ĉ','Ď','ď','Ǵ','ǵ','Ḱ','ḱ','Ĺ','ĺ','Ḿ','ḿ','Ń','ń','Ṕ','ṕ','Ŕ','ŕ','Ś','ś','Ṣ','ṣ','Ş','ş','Ť','ť','Ź','ź','Ẑ','ẑ' ]

ruBasLatnArray = [ 'Sch','sch','Sh','sh','Ch','ch','Zh','zh','Ja','ja','Je','je','Jo','jo','Ju','ju','\'','\"' ]
ruExtLatnArray = [ 'Ŝ','ŝ','Š','š','Č','č','Ž','ž','À','à','È','è','Ò','ò','Ù','ù','′','″' ]

def condenseLatn(inputWord):
	for i in range(len(ruLatnExpArray)):
		inputWord = inputWord.replace(ruLatnExpArray[i],ruLatnCondArray[i])
	return inputWord

def cyrlToLatin(inputWord):
	outputWord = ""
	directCopy = True
	for i in range(len(inputWord)):
		directCopy = True
		for j in range(len(ruCyrlArray)):
			if ruCyrlArray[j] == inputWord[i]:
				directCopy = False
				outputWord += ruLatnArray[j]
				break
		if directCopy == True:
			outputWord += inputWord[i]
	outputWord = condenseLatn(outputWord)
	return outputWord

def basLatnToExt(inputWord):
	for i in range(len(ruBasLatnArray)):
		inputWord = inputWord.replace(ruBasLatnArray[i],ruExtLatnArray[i])
	return inputWord
	
def expandLatin(inputWord):
	for i in range(len(ruLatnCondArray)):
		inputWord = inputWord.replace(ruLatnCondArray[i],ruLatnExpArray[i])
	return inputWord
	
def latinToCyrl(inputWord):
	inputWord = basLatnToExt(inputWord)
	inputWord = expandLatin(inputWord)
	outputWord = ""
	directCopy = True
	for i in range(len(inputWord)):
		directCopy = True
		for j in range(len(ruLatnArray)):
			if ruLatnArray[j] == inputWord[i]:
				directCopy = False
				outputWord += ruCyrlArray[j]
				break
		if directCopy == True:
			outputWord += inputWord[i]
	return outputWord
	
#fullStrCyrl = "правдивость"
#fullStrLatntest = "pravdivost′"
#fullStrExtLatntest = "pravdivosť"

#fullStrLatn = cyrlToLatin(fullStrCyrl)
#fullStrCyrl = latinToCyrl(fullStrLatn)

#print("Original Russian Cyrillic string: ",fullStrCyrl,'\n')
#print("New Latin Russian string: ",fullStrLatn,'\n');

if len(sys.argv) < 2:
	keepRunning = True

	while keepRunning == True:
		print("Do you want to convert from Cyrillic to Latin or Latin to Cyrillic?\n")
		print("Enter EXACTLY \"Latin\" for Latin text output")
		translit = input("or enter EXACTLY \"Cyrillic\" for Cyrillic text output: ")

		#while translit != "Latin" or translit != "Cyrillic":
		#	translit = input("\nEnter EXACTLY either \"Latin\" or \"Cyrillic\": ")

		strInput = input("Enter the string you wish to transliterate: ")
		strOutput = ""

		if translit == "Latin":
			strOutput = cyrlToLatin(strInput)
		if translit == "Cyrillic":
			strOutput = latinToCyrl(strInput)

		print("\nOutput string: ",strOutput,'\n')
	
		restart = input("Do you want to do another? (Yes/No): ")
	
		if restart == "Yes":
			keepRunning = True
		if restart == "No":
			keepRunning = False

elif (len(sys.argv) > 1 and len(sys.argv) < 3) or len(sys.argv) > 4:
	print("Command input usage: ",sys.argv[0]," --to-script={latn,cyrl} \"<Cyrillic/Latin String to transcribe>\"\n")
	print("If 'cyrl' is chosen, enter text in Latin script to transliterate to Cyrillic.")
	print("If 'latn' is chosen, enter text in Cyrillic script to transliterate to Latin.\n")
	print("File input usage: ",sys.argv[0]," --to-script={latn,cyrl} </path/to/inputfile.txt> </path/to/outputfile.txt>\n")
	print("If 'cyrl' is chosen, input file should have Latin script text to transliterate to Cyrillic.")
	print("If 'latn' is chosen, input file should have Cyrillic script text to transliterate to Latin.")
elif len(sys.argv) == 3:
	if sys.argv[1] == '--to-script=latn' or sys.argv[1] == '--to-script=cyrl':
		if sys.argv[1] == '--to-script=latn':
			print(cyrlToLatin(sys.argv[2]))
		elif sys.argv[1] == '--to-script=cyrl':
			print(latinToCyrl(sys.argv[2]))
	else:
		print("Usage: ",sys.argv[0]," --to-script={latn,cyrl} \"<Cyrillic/Latin String to transcribe>\"\n")
		print("If 'cyrl' is chosen, enter text in Latin script to transliterate to Cyrillic.")
		print("If 'latn' is chosen, enter text in Cyrillic script to transliterate to Latin.")
elif len(sys.argv) == 4:
	if sys.argv[1] == '--to-script=latn' or sys.argv[1] == '--to-script=cyrl':
		fileInput = open(sys.argv[2],'r')
		fileOutput = open(sys.argv[3],'w')
		if sys.argv[1] == '--to-script=latn':
			for line in fileInput:
				fileOutput.write(cyrlToLatin(line))
		elif sys.argv[1] == '--to-script=cyrl':
			for line in fileInput:
				fileOutput.write(latinToCyrl(line))
		fileInput.close()
		fileOutput.close()
	else:
		print("File input usage: ",sys.argv[0]," --to-script={latn,cyrl} </path/to/inputfile.txt> </path/to/outputfile.txt>\n")
		print("If 'cyrl' is chosen, input file should have Latin script text to transliterate to Cyrillic.")
		print("If 'latn' is chosen, input file should have Cyrillic script text to transliterate to Latin.")
